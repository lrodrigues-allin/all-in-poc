package br.com.allin.fake.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataEntity {
    @SerializedName("id")
    @Expose
    private Integer id = 0;
    @SerializedName("subject")
    @Expose
    private String subject;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("id_campaign")
    @Expose
    private Integer idCampaign = 0;
    @SerializedName("date")
    @Expose
    private String date = "2019-06-12T03:00:00.000Z";
    @SerializedName("url_scheme")
    @Expose
    private String urlScheme;

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setUrlScheme(String urlScheme) {
        this.urlScheme = urlScheme;
    }
}
