package br.com.allin.fake.ui.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import br.com.allin.fake.R
import kotlinx.android.synthetic.main.activity_remarketing.*

class RemarketingActivity: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        this.setContentView(R.layout.activity_remarketing)
    }

    fun clickBack(view: View) {
        this.finish()
    }

    fun clickBox(view: View) {
        val size = (view.tag as String).toInt()
        val intent = Intent(this, ContactsActivity::class.java)

        if (view == this.ivAbandonoCarrinho) {
            intent.putExtra("title", "Abandono de Carrinho")
            intent.putExtra("subtitle", "Lista de clientes que abandonaram produtos no carrinho:")
            intent.putExtra("size", size)
        } else if (view == this.ivNavegacao) {
            intent.putExtra("title", "Navegação")
            intent.putExtra("subtitle", "Lista de clientes que navegaram em produtos:")
        } else if (view == this.ivRefil) {
            return
//            intent.putExtra("title", "Vendas de Refil")
//            intent.putExtra("subtitle", "Lista de clientes que sempre compram")
        } else if (view == this.ivAniversario) {
            return
//            intent.putExtra("title", "Aniversário")
//            intent.putExtra("subtitle", "Lista de clientes que fazem aniversário esse mês")
        } else if (view == this.ivReconquista) {
            return
//            intent.putExtra("title", "Reconquista")
//            intent.putExtra("subtitle", "Lista de clientes que estão sem comprar")
        } else if (view == this.ivDecididos) {
            return
//            intent.putExtra("title", "Decididos")
//            intent.putExtra("subtitle", "Lista de clientes que navegaram mais que uma vez em um único produto")
        }

        intent.putExtra("size", size)

        this.startActivity(intent)
    }
}