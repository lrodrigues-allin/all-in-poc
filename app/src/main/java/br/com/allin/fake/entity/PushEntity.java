package br.com.allin.fake.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PushEntity {
    @SerializedName("data")
    @Expose
    private DataEntity data;
    @SerializedName("to")
    @Expose
    private String to;

    public PushEntity(DataEntity data, String to) {
        this.data = data;
        this.to = to;
    }
}
