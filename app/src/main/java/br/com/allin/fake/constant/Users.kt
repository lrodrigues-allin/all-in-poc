package br.com.allin.fake.constant

import br.com.allin.fake.entity.UserEntity

val USERS = arrayOf(
        UserEntity(name = "Rafael Rodrigues", phone = "(11) 98696-8444", oportunity = true),
        UserEntity(name = "Felipe Zunino", phone = "(11) 94032-9448"),
        UserEntity(name = "Tiago Moraes", phone = "(11) 99430-0759"),
        UserEntity(name = "Victor Popper", phone = "(11) 99263-6585", oportunity = true),
        UserEntity(name = "Jasmin de Sousa", phone = "(11) 98033-7309"),
        UserEntity(name = "Rafael Araújo", phone = "(11) 96529-1771"),
        UserEntity(name = "Adriano Vasconcelos", phone = "(11) 96462-0852", oportunity = true),
        UserEntity(name = "Thamires Ferreira", phone = "(11) 96398-5089"),
        UserEntity(name = "Rodrigo Oliveira", phone = "(11) 95056-3199", oportunity = true),
        UserEntity(name = "Dayane Vianna", phone = "(11) 98152-8477", oportunity = true),
        UserEntity(name = "Thiago Rodrigues", phone = "(11) 99333-5283"),
        UserEntity(name = "Eduardo Correia", phone = "(11) 98519-6724"),
        UserEntity(name = "Michel Araújo", phone = "(11) 99391-3819"),
        UserEntity(name = "Mario Nascimento", phone = "(11) 97099-3156", oportunity = true),
        UserEntity(name = "Jurandi", phone = "(11) 98469-8597"),
        UserEntity(name = "Marcos", phone = "(11) 99388-1191"),
        UserEntity(name = "Michel Lima", phone = "(11) 98288-6272"),
        UserEntity(name = "Suelen Paiva", phone = "(11) 94892-1322"),
        UserEntity(name = "Lucas Rodrigues", phone = "(11) 98748-2000")
)