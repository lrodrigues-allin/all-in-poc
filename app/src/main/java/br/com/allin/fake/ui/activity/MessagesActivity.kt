package br.com.allin.fake.ui.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import br.com.allin.fake.AlliNApplication
import br.com.allin.fake.R
import br.com.allin.fake.entity.UserEntity
import br.com.allin.fake.ui.adapter.MessagesAdapter
import kotlinx.android.synthetic.main.activity_message.*

class MessagesActivity: AppCompatActivity() {
    var user: UserEntity? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        this.setContentView(R.layout.activity_message)

        this.user = this.intent.getSerializableExtra("user") as UserEntity?

        this.tvTitle.text =this.user!!.name

        this.loadMessages()
    }

    override fun onRestart() {
        super.onRestart()

        this.loadMessages()
    }

    fun clickBack(view: View) {
        this.finish()
    }

    fun clickCreateMessage(view: View) {
        val intent = Intent(this, CreateMessageActivity::class.java)
        intent.putExtra("user", this.user)

        this.startActivity(intent)
    }

    fun loadMessages() {
        val arrayDefaultMessages = AlliNApplication.getMessages(false)
        val arrayCreatedtMessages = AlliNApplication.getMessages(true)

        if (arrayDefaultMessages.size > 0) {
            this.defaultMessages.visibility = View.VISIBLE
            this.rvDefaultMessages.adapter = MessagesAdapter(activity = this, user = this.user!!, messages = arrayDefaultMessages)
        } else {
            this.defaultMessages.visibility = View.GONE
        }

        if (arrayCreatedtMessages.size > 0) {
            this.createdMessages.visibility = View.VISIBLE
            this.rvCreatedMessages.adapter = MessagesAdapter(activity = this, user = this.user!!, messages = arrayCreatedtMessages)
        } else {
            this.createdMessages.visibility = View.GONE
        }
    }
}