package br.com.allin.fake

import android.app.Application
import br.com.allin.mobile.pushnotification.AlliNPush
import br.com.allin.fake.entity.MessageEntity

class AlliNApplication: Application() {
    override fun onCreate() {
        super.onCreate()

        AlliNPush.getInstance().registerForPushNotifications(this)
    }

    companion object {
        private var messages = arrayListOf(
                MessageEntity(message = "Olá ##nome##, não deixe seus produtos esperando, volte e finalize sua compra. acesse: http://allbr.co/5D", fromUser = false),
                MessageEntity(message = "Olá ##nome##, não perca essa oportunidade de finalizar sua compra. Acesse o link: http://allbr.co/5D", fromUser = false),
                MessageEntity(message = "##nome##, hoje é seu dia de sorte, não perca a promoção que estamos hoje, acesse: http://allbr.co/5D", fromUser = false),
                MessageEntity(message = "Não perca essa oportunidade, ##nome##! Só hoje, 5% de desconto em qualquer compra: http://allbr.co/5D", fromUser = false)
        )

        fun getMessages(fromUser: Boolean) : List<MessageEntity> {
            return this.messages.filter {
                it.fromUser == fromUser
            }
        }

        fun addMessage(message: String) : MessageEntity {
            val entity = MessageEntity(message = message, fromUser = true)

            this.messages.add(entity)

            return entity
        }
    }
}