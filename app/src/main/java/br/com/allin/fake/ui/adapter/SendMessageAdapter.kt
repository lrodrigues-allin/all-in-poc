package br.com.allin.fake.ui.adapter

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Adapter
import br.com.allin.fake.R
import br.com.allin.fake.entity.UserEntity
import br.com.allin.fake.ui.activity.EmailActivity
import br.com.allin.fake.ui.activity.MessagesActivity
import br.com.allin.fake.ui.adapter.SendMessageAdapter.SendMessageViewHolder
import kotlinx.android.synthetic.main.adapter_send_message.view.*

class SendMessageAdapter(val activity: Activity, val user: UserEntity, val callback: OnClickCallListener): Adapter<SendMessageViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SendMessageViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.adapter_send_message, parent, false)

        return SendMessageViewHolder(view)
    }

    override fun getItemCount(): Int {
        return this.user.getSenders().size
    }

    override fun onBindViewHolder(holder: SendMessageViewHolder, position: Int) {
        holder.itemView.ivSendMessage.setImageResource(this.user.getSenders()[position])
        holder.itemView.setOnClickListener {
            if (this.user.getSenders()[position] == R.mipmap.box_telefone) {
                callback.clickCall()
            } else if (this.user.getSenders()[position] == R.mipmap.box_whatsapp) {
                val intent = Intent(this.activity, MessagesActivity::class.java)
                intent.putExtra("user", this.user)

                this.activity.startActivity(intent)
            } else if (this.user.getSenders()[position] == R.mipmap.box_email) {
                this.activity.startActivity(Intent(this.activity, EmailActivity::class.java))
            }
        }
    }

    interface OnClickCallListener {
        fun clickCall()
    }

    class SendMessageViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}