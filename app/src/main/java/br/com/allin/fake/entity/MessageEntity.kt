package br.com.allin.fake.entity

import java.io.Serializable

class MessageEntity(val message: String, val fromUser: Boolean) : Serializable