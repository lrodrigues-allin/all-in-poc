package br.com.allin.fake.ui.adapter

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.allin.fake.R
import br.com.allin.fake.entity.UserEntity
import br.com.allin.fake.ui.activity.ClientDataActivity
import kotlinx.android.synthetic.main.adapter_clients.view.*
import kotlinx.android.synthetic.main.adapter_contact.view.tvName
import kotlinx.android.synthetic.main.adapter_contact.view.tvPhone

class ClientsAdapter(val activity: Activity, val users: Array<UserEntity>) : RecyclerView.Adapter<ClientsAdapter.ClientsViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ClientsViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.adapter_clients, parent, false)

        return ClientsViewHolder(view)
    }

    override fun getItemCount(): Int {
        return this.users.size
    }

    override fun onBindViewHolder(holder: ClientsViewHolder, position: Int) {
        holder.itemView.tvName.text = this.users[position].name
        holder.itemView.tvPhone.text = this.users[position].phone

        if (this.users[position].oportunity) {
            holder.itemView.oportunity.visibility = View.VISIBLE
        } else {
            holder.itemView.oportunity.visibility = View.GONE
        }

        holder.itemView.setOnClickListener {
            val intent = Intent(this.activity, ClientDataActivity::class.java)
            intent.putExtra("user", this.users[position])

            this.activity.startActivity(intent)
        }
    }

    class ClientsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}