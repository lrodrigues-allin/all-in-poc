package br.com.allin.fake.ui.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import br.com.allin.fake.R
import br.com.allin.fake.entity.UserEntity
import kotlinx.android.synthetic.main.activity_client_data.*

class ClientDataActivity : AppCompatActivity() {
    var user: UserEntity? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        this.setContentView(R.layout.activity_client_data)

        this.user = this.intent.getSerializableExtra("user") as UserEntity?

        this.tvTitle.text =this.user!!.name

        if (this.user!!.oportunity) {
            this.oportunity.visibility = View.VISIBLE
        } else {
            this.oportunity.visibility = View.GONE
        }
    }

    fun clickBack(view: View) {
        this.finish()
    }

    fun clickSeeProducts(view: View) {
        val intent = Intent(this, ProductsActivity::class.java)
        intent.putExtra("user", this.user)

        this.startActivity(intent)
    }

    fun clickContact(view: View) {
        val intent = Intent(this, SendMessageActivity::class.java)
        intent.putExtra("user", this.user)

        this.startActivity(intent)
    }
}