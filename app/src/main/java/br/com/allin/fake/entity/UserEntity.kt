package br.com.allin.fake.entity

import br.com.allin.fake.R
import br.com.allin.fake.constant.USERS
import java.io.Serializable

class UserEntity(val name: String, val phone: String, val oportunity: Boolean = false) : Serializable {
    companion object {
        fun get(size: Int): List<UserEntity> {
            val users = ArrayList<UserEntity>()

            for (index in 0..USERS.size - 1) {
                users.add(USERS[index])

                if (users.size == size) {
                    break
                }
            }

            return users
        }
    }

    fun getSenders(): List<Int> {
        var list = ArrayList<Int>()

//        if (!TextUtils.isEmpty(this.phone)) {
        list.add(R.mipmap.box_whatsapp)
//        }

//        if (!TextUtils.isEmpty(this.email)) {
        list.add(R.mipmap.box_email)
//        }

//        if (!TextUtils.isEmpty(this.phone)) {
        list.add(R.mipmap.box_telefone)
//        }

        return list
    }
}