package br.com.allin.fake.ui.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import android.view.View
import br.com.allin.mobile.pushnotification.AlliNPush
import br.com.allin.fake.R
import br.com.allin.fake.constant.USERS
import br.com.allin.fake.entity.DataEntity
import br.com.allin.fake.entity.PushEntity
import br.com.allin.fake.entity.PushResponseEntity
import br.com.allin.fake.service.PushService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.net.URLEncoder

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)


    }

    fun clickRemarketing(view: View) {
        this.startActivity(Intent(this, RemarketingActivity::class.java))
    }

    fun clickClients(view: View) {
        this.startActivity(Intent(this, ClientsActivity::class.java))
    }

    // SEND PUSH
    fun clickCart(view: View) {
        val size = 5
        val title = URLEncoder.encode("Abandono de Carrinho", "UTF-8")
        val subtitle = URLEncoder.encode("Lista de clientes que abandonaram produtos no carrinho:", "UTF-8")

        val data = DataEntity()
        data.setSubject("Oportunidade de venda! \ud83d\udcb0 \ud83d\udcb0")
        data.setDescription("Corre! ${size} clientes abandonaram o carrinho na sua loja \ud83d\uded2 FALE AGORA COM ELES!")
        data.setUrlScheme("allinpoc://remarketing?title=$title&subtitle=$subtitle&size=$size")
        val push = PushEntity(data, AlliNPush.getInstance().deviceToken)

        this.sendPush(push)
    }

    fun clickNavigation(view: View) {
        val size = 3
        val title = URLEncoder.encode("Navegação", "UTF-8")
        val subtitle = URLEncoder.encode("Lista de clientes que navegaram em produtos:", "UTF-8")

        val data = DataEntity()
        data.setSubject("Oportunidade de venda! \ud83d\udcb0 \ud83d\udcb0")
        data.setDescription("Dê um oi para os $size clientes que navegaram na sua loja virtual! Ajude eles a CONCLUÍREM A COMPRA!")
        data.setUrlScheme("allinpoc://remarketing?title=$title&subtitle=$subtitle&size=$size")
        val push = PushEntity(data, AlliNPush.getInstance().deviceToken)

        this.sendPush(push)
    }

    private fun sendPush(push: PushEntity) {
        val key = "key=AAAAVlfgnC8:APA91bHonXAMcRIi7KVSYQ_WepO7QXt9TxSCikeFM_XcpP1EKpUMdqQTlNhz8NXIjtz7RP6KuBvfyzRUX1eq1boJCagOp6pMmTiZAosMQ-fNJjwd2_IFv8Ahtbrfwsbcd7aVWRi6vqAr"

        Retrofit.Builder()
                .baseUrl("https://fcm.googleapis.com/fcm/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(PushService::class.java)
                .sendPush(key, push)
                .enqueue(object : Callback<PushResponseEntity> {
                    override fun onResponse(call: Call<PushResponseEntity>, response: Response<PushResponseEntity>) {
                    }

                    override fun onFailure(call: Call<PushResponseEntity>, t: Throwable) {
                    }
                })
    }
}
