package br.com.allin.fake.ui.adapter

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import br.com.allin.fake.R
import br.com.allin.fake.entity.UserEntity
import br.com.allin.fake.ui.activity.ProductsActivity
import br.com.allin.fake.ui.activity.SendMessageActivity
import br.com.allin.fake.ui.adapter.ContactsAdapter.ContactsViewHolder
import kotlinx.android.synthetic.main.adapter_contact.view.*

class ContactsAdapter(val activity: Activity, val users: List<UserEntity>) : Adapter<ContactsViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactsViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.adapter_contact, parent, false)

        return ContactsViewHolder(view)
    }

    override fun getItemCount(): Int {
        return this.users.size
    }

    override fun onBindViewHolder(holder: ContactsViewHolder, position: Int) {
        holder.itemView.tvName.text = users[position].name
        holder.itemView.tvPhone.text = users[position].phone
        holder.itemView.ivSeeProducts.setOnClickListener {
            val intent = Intent(this.activity, ProductsActivity::class.java)
            intent.putExtra("user", users[position])

            this.activity.startActivity(intent)
        }
        holder.itemView.ivSendMessage.setOnClickListener {
            val intent = Intent(this.activity, SendMessageActivity::class.java)
            intent.putExtra("user", users[position])

            this.activity.startActivity(intent)
        }
    }

    class ContactsViewHolder(itemView: View) : ViewHolder(itemView)
}