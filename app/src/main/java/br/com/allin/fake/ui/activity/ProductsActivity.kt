package br.com.allin.fake.ui.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import br.com.allin.fake.R
import br.com.allin.fake.entity.UserEntity
import kotlinx.android.synthetic.main.activity_message.*

class ProductsActivity : AppCompatActivity() {
    var user: UserEntity? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        this.setContentView(R.layout.activity_products)

        this.user = this.intent.getSerializableExtra("user") as UserEntity?

        this.tvTitle.text =this.user!!.name
    }

    fun clickBack(view: View) {
        this.finish()
    }

    fun clickCreateMessage(view: View) {
        val intent = Intent(this, SendMessageActivity::class.java)
        intent.putExtra("user", this.user)

        this.startActivity(intent)
    }
}