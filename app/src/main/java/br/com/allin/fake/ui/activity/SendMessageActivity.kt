package br.com.allin.fake.ui.activity

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import br.com.allin.fake.R
import br.com.allin.fake.entity.UserEntity
import br.com.allin.fake.ui.adapter.SendMessageAdapter
import kotlinx.android.synthetic.main.activity_message.tvTitle
import kotlinx.android.synthetic.main.activity_send_message.*

class SendMessageActivity : AppCompatActivity(), SendMessageAdapter.OnClickCallListener {
    private val PHONE_PERMISSION = 1

    var user: UserEntity? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        this.setContentView(R.layout.activity_send_message)

        this.user = this.intent.getSerializableExtra("user") as UserEntity?

        this.tvTitle.text =this.user!!.name

        this.rvSendMessage.layoutManager = GridLayoutManager(this, 2)
        this.rvSendMessage.adapter = SendMessageAdapter(activity = this, user = this.user!!, callback = this)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == PHONE_PERMISSION) {
            if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:${this.user!!.phone}"))
                this.startActivity(intent)
            }
        }
    }

    override fun clickCall() {
        val permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE)

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CALL_PHONE), PHONE_PERMISSION)
        } else {
            val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:${this.user!!.phone}"))
            this.startActivity(intent)
        }
    }

    fun clickBack(view: View) {
        this.finish()
    }
}