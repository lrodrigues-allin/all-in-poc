package br.com.allin.fake.service

import br.com.allin.fake.entity.PushEntity
import br.com.allin.fake.entity.PushResponseEntity
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.POST

interface PushService {
    @Headers("Content-Type: application/json")
    @POST("send")
    fun sendPush(@Header("Authorization") key: String, @Body push: PushEntity): Call<PushResponseEntity>
}