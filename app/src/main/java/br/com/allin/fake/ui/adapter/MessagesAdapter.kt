package br.com.allin.fake.ui.adapter

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Adapter
import br.com.allin.fake.R
import br.com.allin.fake.entity.MessageEntity
import br.com.allin.fake.entity.UserEntity
import br.com.allin.fake.ui.adapter.MessagesAdapter.MessagesViewHolder
import kotlinx.android.synthetic.main.adapter_message.view.*
import java.net.URLEncoder


class MessagesAdapter(val activity: Activity, var user: UserEntity, val messages: List<MessageEntity>) : Adapter<MessagesViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessagesViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.adapter_message, parent, false)

        return MessagesViewHolder(view)
    }

    override fun getItemCount(): Int {
        return this.messages.size
    }

    override fun onBindViewHolder(holder: MessagesViewHolder, position: Int) {
        val message = this.messages[position]

        holder.itemView.tvMessage.text = message.message
        holder.itemView.btMessage.setOnClickListener {
            val finalMessage = message.message.replace("##nome##", this.user.name)
            val phone = this.user.phone
                    .replace("(", "")
                    .replace(")", "")
                    .replace("-", "")
                    .replace(" ", "")
            val messageEncoded = URLEncoder.encode(finalMessage, "UTF-8")
            val url = "https://api.whatsapp.com/send?phone=+55$phone&text=$messageEncoded"

            this.activity.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
        }
    }

    class MessagesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}