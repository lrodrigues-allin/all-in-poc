package br.com.allin.fake.ui.activity

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import br.com.allin.fake.R
import br.com.allin.fake.constant.USERS
import br.com.allin.fake.ui.adapter.ClientsAdapter
import kotlinx.android.synthetic.main.activity_clients.*

class ClientsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        this.setContentView(R.layout.activity_clients)

        this.rvClients.adapter = ClientsAdapter(activity = this, users = USERS)
    }

    fun clickBack(view: View) {
        this.finish()
    }
}