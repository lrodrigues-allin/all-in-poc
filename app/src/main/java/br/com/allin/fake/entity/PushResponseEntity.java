package br.com.allin.fake.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PushResponseEntity {
    @SerializedName("multicast_id")
    @Expose
    private Long multicastId;
    @SerializedName("success")
    @Expose
    private Long success;
    @SerializedName("failure")
    @Expose
    private Long failure;
    @SerializedName("canonical_ids")
    @Expose
    private Long canonicalIds;
    @SerializedName("results")
    @Expose
    private List<ResultEntity> results = null;

    public Long getMulticastId() {
        return multicastId;
    }

    public Long getSuccess() {
        return success;
    }

    public Long getFailure() {
        return failure;
    }

    public Long getCanonicalIds() {
        return canonicalIds;
    }

    public List<ResultEntity> getResults() {
        return results;
    }
}
