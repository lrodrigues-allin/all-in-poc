package br.com.allin.fake.ui.activity

import android.os.Bundle
import android.text.TextUtils
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import br.com.allin.fake.R
import br.com.allin.fake.entity.UserEntity
import br.com.allin.fake.ui.adapter.ContactsAdapter
import kotlinx.android.synthetic.main.activity_contacts.*

class ContactsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_contacts)

        this.buildIntent()
    }

    fun clickBack(view: View) {
        this.finish()
    }

    private fun buildIntent() {
        val uri = intent.data
        val title: String?
        val subtitle: String?
        val size: Int

        if (uri != null) {
            val host = uri.host

            if (host == null || TextUtils.isEmpty(host)) {
                return
            }

            title = uri.getQueryParameter("title")
            subtitle = uri.getQueryParameter("subtitle")
            size = Integer.parseInt(uri.getQueryParameter("size")!!)
        } else {
            title = this.intent.getStringExtra("title")
            subtitle = this.intent.getStringExtra("subtitle")
            size = this.intent.getIntExtra("size", 0)
        }

        this.tvTitle.text = title
        this.tvSubtitle.text = subtitle
        this.rvContacts.adapter = ContactsAdapter(activity = this, users = UserEntity.get(size))
    }
}