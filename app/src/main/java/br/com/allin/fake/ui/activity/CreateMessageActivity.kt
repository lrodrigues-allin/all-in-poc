package br.com.allin.fake.ui.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import br.com.allin.fake.AlliNApplication
import br.com.allin.fake.R
import br.com.allin.fake.entity.UserEntity
import kotlinx.android.synthetic.main.activity_create_message.*
import kotlinx.android.synthetic.main.activity_message.tvTitle
import android.text.Spanned
import android.text.SpannableStringBuilder
import android.graphics.Typeface
import android.net.Uri
import android.text.TextUtils
import android.widget.Toast
import br.com.allin.fake.ui.custom.CustomTypefaceSpan
import java.net.URLEncoder


class CreateMessageActivity: AppCompatActivity() {
    var user: UserEntity? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        this.setContentView(R.layout.activity_create_message)

        this.user = this.intent.getSerializableExtra("user") as UserEntity?

        this.tvTitle.text =this.user!!.name

        this.rbSemDesconto.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                this.rbCincoDesconto.isChecked = false
                this.rbDezDesconto.isChecked = false
            }

            this.etMessage.setText(this.clearMessage())
        }

        this.rbCincoDesconto.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                this.rbSemDesconto.isChecked = false
                this.rbDezDesconto.isChecked = false
            }

            this.addDiscount(5)
        }

        this.rbDezDesconto.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                this.rbSemDesconto.isChecked = false
                this.rbCincoDesconto.isChecked = false
            }

            this.addDiscount(10)
        }
    }

    fun clickBack(view: View) {
        this.finish()
    }

    fun clickSave(view: View) {
        if (TextUtils.isEmpty(this.etMessage.text.toString())) {
            Toast.makeText(this, "Digite uma mensagem para ser salva", Toast.LENGTH_LONG).show()

            return
        }

        val message = AlliNApplication.addMessage(this.etMessage.text.toString())
        val finalMessage = message.message.replace("##nome##", this.user!!.name)
        val phone = this.user!!.phone
                .replace("(", "")
                .replace(")", "")
                .replace("-", "")
                .replace(" ", "")
        val messageEncoded = URLEncoder.encode(finalMessage, "UTF-8")
        val url = "https://api.whatsapp.com/send?phone=+55$phone&text=$messageEncoded"

        this.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
        this.finish()
    }

    private fun clearMessage() : String {
        val message = this.etMessage.text.toString().trim()

        return message.replace("com 5% de desconto", "")
                .replace("com 10% de desconto", "").trim()
    }

    private fun addDiscount(value: Int) {
        val messageCleared = this.clearMessage()
        val discount = "com ${value}% de desconto"
        val message = "${messageCleared} ${discount}"

        val naturaRegular = Typeface.createFromAsset(this.assets, "naturasans_regular.otf")
        val naturaBold = Typeface.createFromAsset(this.assets, "naturasans_bold.otf")
        val spannable = SpannableStringBuilder(message)

        spannable.setSpan(CustomTypefaceSpan("", naturaRegular), 0, messageCleared.length, Spanned.SPAN_EXCLUSIVE_INCLUSIVE)
        spannable.setSpan(CustomTypefaceSpan("", naturaBold), messageCleared.length, messageCleared.length + discount.length + 1, Spanned.SPAN_EXCLUSIVE_INCLUSIVE)

        this.etMessage.text = spannable
    }
}